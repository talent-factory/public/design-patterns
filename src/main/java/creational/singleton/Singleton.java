package creational.singleton;

/**
 * Implementation des Singleton Design Patterns. Es wird sichergestellt,
 * dass nur eine Instanz dieser Klasse erzeugt werden kann.
 */
public class Singleton {

    /**
     * Singleton Objekt.
     */
    private static Singleton instance;

    /**
     * Dieser private Konstruktor stellt sicher, dass die Instanz nur
     * innerhalb dieser Klasse erstellt werden kann.
     */
    private Singleton() {
    }

    /**
     * Liefert die einzige Instanz dieser Klasse. Die
     * <a href="https://en.wikipedia.org/wiki/Lazy_initialization">lazy initialization</a>
     * Technik wird an dieser Stelle verwendet um ein Objekt er dann zu erzeugen,
     * wenn es verwendet wird.
     *
     * @return liefert das Singleton Objekt
     */
    @SuppressWarnings("InstantiationOfUtilityClass")
    public static Singleton getInstance() {
        if (instance == null)
            instance = new Singleton();
        return instance;
    }
}
