# Singleton

### Zweck
Sichere ab, dass eine Klasse genau ein Exemplar besitzt und stelle einen globalen Zugriffspunkt darauf bereit.

### Motivation
Bei manchen Klassen ist es wichtig, dass es genau ein Exemplar gibt. Obwohl es in einem System viele Drucker geben kann sollte es nur einen Drukerspooler geben. Es sollte nur ein Dateisystem und nur eine Fensterverwaltung geben.

### Anwendbarkeit

Verwenden Sie das Singleton, wenn

- es genau ein Exemplar einer Klasse geben und es für Klienten an einem wohldefinierten Punkt zugreifbar sein muss.
- das einzige Exemplar durch Bildung von Unterklassen erweiterbar sein soll und Klienten in der Lage sein sollten, das erweiterte Exemplar ohne Modifikation ihres Codes verwenden können.

### Struktur

![UML](../../../resources/Singleton.png)

### Interaktionen

Klienten greifen auf ein Singletonexemplar ausschliesslich durch die zur Verfügung gestellt Methode (`getInstance()`) zu.

### Implementierung

Die Implementierung besteht im Wesentlichen aus einer Klasse, welche in einer Projektumgebung noch aus weiteren Eigenschaften und Methoden bestehen kann.

- [Singleton.java](Singleton.java)
- [Main.java](Main.java)
