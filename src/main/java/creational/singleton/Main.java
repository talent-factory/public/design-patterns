package creational.singleton;

/**
 * Mit dieser Klasse stellt einen einfachen Klienten dar, welche
 * ein Objekt der Singleton Klasse verwenden möchte,
 */
public class Main {

    /**
     * Startpunkt unsers Programmes.
     *
     * @param args wird nicht verwendet.
     */
    public static void main(String[] args) {

        Singleton singleton = Singleton.getInstance();

        // Verwenden des Objektes...

    }
}
