package structural.proxy;

import lombok.Data;

/**
 * Einfache Klasse zur Repräsentation eines Benutzers, welcher sich
 * über unser Proxy identifizieren möchte.
 */
@Data
public class User {

    /**
     * Name des Benutzers.
     */
    private final String userName;

    public User(String userName) {
        this.userName = userName;
    }
}
