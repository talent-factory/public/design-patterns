package structural.proxy;

import java.io.IOException;

/**
 * Gemeinsame Schnittstelle zwischen dem Proxy und dem eigentlichen Subjekt,
 * welches die gewünschten Aktionen/Methoden ausführen wird. In diesem Beispiel
 * verwenden wir eine einfache TCP/IP Kommunikatin zwischen einem Klienten und
 * einem Server.
 */
public interface Subject {

    /**
     * TCP/IP Port für die Kommunikation
     */
    int PORT = 8765;

    /**
     * Initiieren eines Anmeldevorganges.
     *
     * @param user Anmeldename
     * @return TRUE, falls die Anmeldung erfolgreich war
     * @throws IOException Fehler in der Kommunikation
     */
    boolean login(User user) throws IOException;
}
