package structural.proxy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.java.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.RemoteException;

/**
 * Einfacher Server für die Demonstration einer TCP/IP Kommunikation
 * mit einem Client.
 */
@Log
public class LoginService extends Thread implements Subject {

    /**
     * Socket-Objekt für die TCP/IP Kommunikation.
     */
    private final Socket client;

    /**
     * Für das Lesen/Schreiben von JSON Objekten.
     */
    private final Gson gson = new GsonBuilder().create();

    /**
     * Konstruktor für unseren Echo-Server.
     *
     * @param socket zu vernendendes Socket-Objekt für die TCP/IP Kommunikation.
     */
    public LoginService(final Socket socket) {
        this.client = socket;
    }

    /**
     * Startet einen Server auf dem konfigurierten Port (8765). In einer
     * ewigen Schleife wartet der Server auf eine Meldung des Client. Sobald
     * sich ein Client meldet wird in einem separaten Thread die Kommunikation
     * mit diesem Client aufgenommen.
     *
     * @param args wird aktuell nicht verwendet
     */
    public static void main(final String[] args) {
        try (ServerSocket server = new ServerSocket(PORT)) {
            log.info("EchoServer auf " + PORT + " gestartet ...");
            while (true) {
                Socket client = server.accept();
                new LoginService(client).start();
            }
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
    }

    /**
     * Eigentlicher Start des Threads. Diese Methode wird implizit aus der
     * main() aufgerufen.
     */
    @Override
    public void run() {
        try (BufferedReader in =
                     new BufferedReader(
                             new InputStreamReader(client.getInputStream()));
             PrintWriter client =
                     new PrintWriter(
                             this.client.getOutputStream(), true)) {

            String input;
            while ((input = in.readLine()) != null) {
                log.info(input);
                User user = gson.fromJson(input, User.class);
                client.println(
                        user.getUserName().equals("admin") ? "ok" : "nok");
            }
        } catch (IOException e) {
            log.severe(e.getMessage());
        }
    }

    /**
     * Initiieren eines Anmeldevorganges.
     *
     * @param user Anmeldename
     * @return TRUE, falls die Anmeldung erfolgreich war
     * @throws RemoteException Fehler in der Kommunikation
     */
    @Override
    public boolean login(User user) throws RemoteException {
        return false;
    }
}
