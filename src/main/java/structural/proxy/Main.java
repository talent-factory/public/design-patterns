package structural.proxy;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        User user = new User("admin");

        Subject subject = new Proxy("127.0.0.1");

        if (subject.login(user)) {
            System.out.println("Erfolgreich angemeldet!");
            System.exit(0);
        }

        System.out.println("Bitte später noch einmal versuchen.");

    }
}
