# Proxy

### Zweck

<p>Kontrolliere den Zugriff auf ein Objekt eines vorgelagerten
Stellvertreterobjekts.
</p>

### Motivation
<p>
Ein Grund, um den Zugriff auf ein Objekt zu kontrollieren, besteht in
der Möglichkeit, die vollen Kosten seiner Erzeugung und Initialisierung
soweit zu verzögern, bis wir das Objekt tatsächlich benutzen wollen.
Stellen Sie sich einen Dokumenteneditor vor, der grafische Objekte in
einem Dokument einbetten kann.
</p>

<p>
Die Erzeugung mancher grafischer Objekte, wie grosse gerasterte Bilder,
kann sehr teuer sein. Das Öffnen eines Dokumentes sollte aber schnell
geschehen, so dass wir es vermeiden sollten, alle teuren Objekte auf
einmal zu erzeugen.
</p>

![UML](../../../resources/BildProxy.png)

<p>Die Lösung besteht darin, ein anderes Objekt, ein <b>Proxy</b>, anstelle
des Bildes zu verwenden, das als Platzhalter für das richtige Bild fungiert.
Das <code>BildProxy</code> erzeugt das tatsächliche Bild nur dann, wenn ihm der
Dokumenteditor mittels Aufruf dazu auffordert. Das Proxy leitet weitere
Operationsaufrufe direkt an das Bild weiter. Es muss daher nach seiner
Erzeugung eine Referent auf das Bild behalten.</p>

### Anwendbarkeit

<p>Das Proxymuster ist anwendbar, sobald es den Bedarf nach einer
anpassungsfähigeren und intelligenteren Referenz auf ein Objekt als einen
einfachen Zeiger gibt. Hier einige Beispiele:</p>

- <p>Ein <code>RemoteProxy</code> stellt einen lokalen Stellvertreter für
ein Objekt in einem anderen Adressraum dar.</p>

- <p>Ein <code>SchutzProxy</code> kontrolliert den Zugriff auf das
Originalobjekt. Schutzproxies sind nützlich, wenn Objekte über unterschiedliche  
Zugriffsrechte verfügen sollen.</p>

### Struktur

### Interaktionen

### Implementierung
