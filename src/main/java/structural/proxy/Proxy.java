package structural.proxy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Einfacher Client für die Demonstration einer TCP/IP Kommunikation
 * mit einem Server.
 */
public class Proxy implements Subject {

    /**
     * Für das Lesen/Schreiben von JSON Objekten.
     */
    private final Gson gson = new GsonBuilder().create();

    private final BufferedReader in;
    private final PrintWriter out;

    public Proxy(String host) throws IOException {

        /*
         * Bereitstellen der benötigten Kommunikationsobjekte.
         */
        Socket socket = new Socket(host, PORT);
        in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(
                socket.getOutputStream(), true);
    }

    /**
     * Initiieren eines Anmeldevorganges.
     *
     * @param user Anmeldename
     * @return TRUE, falls die Anmeldung erfolgreich war
     * @throws IOException Fehler in der Kommunikation
     */
    @Override
    public boolean login(User user) throws IOException {
        out.println(gson.toJson(user));
        return "ok".equals(in.readLine());
    }
}
