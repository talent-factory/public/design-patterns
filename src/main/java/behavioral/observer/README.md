# Observer

### Auch bekannt als
Dependents, Publish-Subscribe

### Zweck
<p>Definiere eine <i>1-zu-n</i> Abhängigkeit zwischen Objekten, so dass
die Änderung des Zustands eines Objekts dazu führt, dass alle
abhängigen Objekte benachrichtigt und automatisch aktualisiert
werden.
</p>

### Motivation
<p>Teilt man ein System in eine Menge von interagierenden Klassen auf,
so ergibt sich häufig der Nebeneffekt, dass die Konsistenz zwischen den
miteinander in Beziehung stehenden Objekten aufrechterhalten werden
muss. Man möchte diese Konsistenz üblicherweise nicht dadurch
sicherstellen, dass man die Klassen eng miteinander koppelt, weil dies
ihre Wiederverwendbarkeit einschränkt.</p>

### Anwendbarkeit
Verwenden Sie dieses Muster in jeder der folgenden Situationen:

- Wenn eine Abstraktion zwei Aspekte besitzt, von denen der eine von dem anderen abhängt. Die Kapselung dieser Aspekte in unterschiedlichen  Objekten ermöglicht es Ihnen, sie zu variieren und sie unabhängig  voneinander wiederzuverwenden.
- Wenn die Änderung eines Objekts die Änderung anderer Objekte verlangt und Sie nicht wissen, wieviele Objekte geändert werden müssen.
- Wenn ein Objekt in der Lage sein sollte, andere Objekte zu benachrichtigen, ohne Annahme darüber treffen zu dürfen, wer diese anderen Objekte sind. Mit anderen Worten: Sie wollen diese Objekte nicht eng miteinander koppeln.

### Struktur

![UML](../../../resources/Observer.png)

#### Beispiel
<p>In einem weit entfernten Land leben die Rassen der Hobbits und Orks.
Beide halten sich meist im Freien auf, so dass sie den Wetteränderungen
genau folgen. Man könnte sagen, dass sie das Wetter ständig beobachten.</p>
