# Memento

<p>Erfasse und externalisiere den inneren Zustand eines Objekts,
ohne seine Kapslung zu verletzen, so dass das Objekt später in diesen
Zustand zurückversetzt werdenb kann.</p>

### Motivation

<p>Mitunter muss der interne Zustand eines Objekts festgehalten werden.
Dies wird benötigt, wenn man Haltepunkte und <code>UNDO</code>-Mechanismen
implementiert, welche es Benutzern ermöglichen, von probeweise
ausgeführten Operationen wieder zurückzukehren. Sie müssen den Zustand
irgendwo zwischenspeichern (<i>history</i>).</p>

### Anwendbarkeit

Verwenden Sie das Mementomusterm wenn

- eine Momentaufnahme des Zustands eines Objekts zwischengespeichert werden muss, so dass es zu einem späteren Zeitpunkt in diesen Zustand zurückversetzt werden kann.
- eine direkte Schnittstelle zum Ermitteln des Zustands die Implementierngsdetails offenlegen und die Kapselung des Objekts  aufbrechen würde.

### Struktur

![UML](../../../resources/Memento.png)
