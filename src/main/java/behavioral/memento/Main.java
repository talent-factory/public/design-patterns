package behavioral.memento;

/**
 * Mit dieser Hilfsklasse zeigen wir die Funktionalität des
 * <a href="https://java-design-patterns.com/patterns/memento/">Memento</a>
 * Entwurfsmusters.
 */
public class Main {

    public static void main(String[] args) {

        /*
         * Ganz im Sinne des Single Responsibility Principle erledigen
         * die folgenden Objekte nur EINE Aufgabe.
         */
        Editor editor = new Editor();    // Editierfunktionen
        History history = new History(); // Verwalten der 'History'

        editor.setContent("a"); // Neuer Zustande welcher historisiert werden soll
        history.push(editor.createState());

        editor.setContent("b"); // Neuer Zustande welcher historisiert werden soll
        history.push(editor.createState());

        editor.setContent("c");
        System.out.println(editor); // Ausgabe von 'c'

        editor.undo(history.pop());
        System.out.println(editor); // Ausgabe von 'b'

        editor.undo(history.pop());
        System.out.println(editor); // Ausgabe von 'a'      
    }
}
