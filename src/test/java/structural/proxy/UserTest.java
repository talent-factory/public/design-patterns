package structural.proxy;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Kleiner Test zur Überprüfung der JSON Bibliothek.
 */
class UserTest {

    private final Gson gson = new GsonBuilder().create();
    private User user;

    @BeforeEach
    void setUp() {
        user = new User("admin");
    }

    @Test
    void toJson() {
        assertEquals("{\"userName\":\"admin\"}", gson.toJson(user));
    }

}
