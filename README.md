# Die Entstehungsgeschichte der Entwurfsmuster

Der breiten Öffentlichkeit wurden Design Patterns im Jahr 1995 vorgestellt. Die vier Autoren (Gang of Four) Erich Gamma, Richard Helm, Ralph Johnson und John Vlissides gelten als Wegbereiter dieser revolutionären Software-Entwurfsidee. Sie haben in ihrem Buch [Design Patterns. Elements of Reusable Object-Oriented Software](https://books.google.de/books/about/Design_Patterns.html?id=6oHuKQe3TjQC&redir_esc=y) oft benötigte Lösungsmuster für wichtige Programmierprobleme allgemein beschrieben.

<p>Mit ihrem Werk über Software-Entwurfsmuster haben sie eine der
bedeutsamsten und hilfreichsten Entwicklungen der objektorientierten
Programmierung der späten 90er losgetreten und es überhaupt erst
ermöglicht standardisierte Bezeichnungen für bestimmte Softwaredesigns
zu finden.</p>

<p>Seit dem Erscheinen ihrer Buchs werden Begriffe wie Singleton,
Factory oder Iterator routinemäßig von Programmieren bei der
Beschreibung von objektorientierten Softwareprojekten verwendet und
förderten die Kommunikationsfähigkeit der Entwickler untereinander
erheblich.</p>

<p>Der Einfachheit halber werden wir für die einzelnen vorgestellten
Patterns keine deutschen Übersetzungen, sondern die Originalbezeichnungen
gemäss Literatur verwenden.</p>

### Beschreibung von Entwurfsmustern

<p>Wir beschreiben Entwurfsmuster mit Hilfe eines einheitlichen Formats,
nachdem die Muster beschrieben werden.</p>

- Name Entwurfsmuster / Klassifizierung
- Zweck
- Auch bekannt als
- Motivation
- Anwendbarkeit
- Struktur
- Interaktionen
- Implementierung


## Struktur diese Entwurfsmuster

<p>Wir klassifizieren Muster mittels zweier Kriterien. Das erste
Kriterium, die Aufgabe, gibt wieder, was das Muster macht. Muster
können entweder eine <b>erzeugende</b>, eine <b>strukturorientierte</b>
oder eine <b>verhaltensorientierte</b> Aufgabe haben.</p>

- [Erzeugungsmuster](src/main/java/creational/)
- [Strukturmuster](src/main/java/structural/)
- [Verhaltensmuster](src/main/java/behavioral/)


## Referenzen

- [Java Design Pattern](https://java-design-patterns.com/)
- [GoF Pattern](https://www.gofpatterns.com/)
- [Tutorialspoint](https://www.tutorialspoint.com/design_pattern/design_pattern_overview.htm)
- [Spring Framework Guru](https://springframework.guru/gang-of-four-design-patterns/)
